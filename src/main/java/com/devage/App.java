package com.devage;

/**
 * Hi there!
 * @author Zankiv Oleh
 * @version 1.0
 * @since 07.11.2019
 */
public class App {
    /**
     * @method main for you
     * @param args of main
     */
    public static void main(final String[] args) {
        System.out.println("Hi there!");
        Fibonacci fibonacci = new Fibonacci();
    }
}
